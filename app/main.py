import logging
import time
from random import randrange
from pathlib import Path

"""
Example log to ingest
[java-.x-eventloop-thread-4] 2021-12-13T16:30:05.946+01:00 INFO [org.composant.controller.WebSocketController]  Closed WebSocket
"""

logfile = './logs/app.log'

format = ('[java.x-eventloop-thread-3] '
          # '%(asctime)s '
          '%(asctime)s.%(msecs)03d'
          '+01:00 '
          '%(levelname)s '
          '[fr.org.module.name.moduleController] '
          '%(message)s')

Path(logfile).touch()

logging.basicConfig(
    level=logging.INFO,
    format=format,
    datefmt='%Y-%m-%dT:%H:%M:%S',
    handlers=[
        logging.FileHandler(logfile),
        logging.StreamHandler()
    ])

logging.info("========== app log ========== ")
logging.info("Press Enter to create info or custom pattern")
logging.info("Press 1 to create warning")
logging.info("Press 2 to create severe")
logging.info("Press 3 to dump trace")
logging.info("Press 4 to post session")
logging.info("Press 5 to trigger alert session")

running = True

while running:
    sessionInfo = "Find session time : " + str(randrange(999)) + " ms."
    sessionCritical = "Find session time : " + str(randrange(99999)) + " ms."
    try:
        time.sleep(randrange(10))
        text = "4"
        if text == "1":
            logging.warning("A warning occured!")
        elif text == "2":
            logging.error("A severe occured!")
        elif text == "3":
            raise Exception("An error occured!")
        elif text == "4":
            logging.info(sessionInfo)
        elif text == "5":
            logging.info(sessionCritical)
        else:
            logging.info(text)
    except Exception as e:
        logging.exception(e)
    except KeyboardInterrupt:
        logging.info("Exiting")
        running = False
