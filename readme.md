# README

## SETUP

* Promtail to push logs and expose extracted metrics
* Loki to store and ingest logs
* Prometheus to store and ingest metrics
* Alertmanager to fire alert to slack
* Grafana to visualize logs and metrics
* main.py to generate log lines

## RUN

### Server side

```
docker-compse up -d
```

### App side

* Run `python3 main.py` to write log lines.


## Alerts

Alert are send when `app_session_time_bucket` of 2000 ms proportion > 50% of all `app_session_time_count`.

Bucket definition (ms)
`buckets: [500, 750, 1000, 1500, 2000, 2500]`

```
promtail_custom_app_session_time_bucket{le="2000.0"} / ignoring (le) promtail_custom_app_session_time_count < 0.5
```

### Check alert 

With web ui

* http://localhost:9093/#/alerts

With amtool

```
user@server ~❯ docker-compose exec alertmanager amtool --alertmanager.url= alert
Alertname         Starts At                Summary  State
FindSessionTime   2021-12-15 16:08:12 UTC           active
```

With slack channel

![slack](slack.png)

## Grafana Dashboard

![dashboard](dashboard.png)