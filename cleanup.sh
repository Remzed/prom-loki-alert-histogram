#!/usr/bin/env bash

docker-compose down 
rm -rf loki/chunks
rm -rf loki/index
rm -rf loki/wal
rm -rf prometheus/01*
rm -rf prometheus/chunks_head
rm -rf prometheus/queries.active
rm -rf prometheus/lock
rm -rf prometheus/wal
rm -rf logs/*
